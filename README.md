Bienvenido a Express

# Curso Desarrollo Lado Servidor Nodejs Express Mongodb

Este es el repositorio de Sebastián Guerrero para el curso de Coursera Curso Desarrollo Lado Servidor Nodejs Express Mongodb

## Installation

Ingresa a la terminal (cmd) e ingresa el siguiente código

```bash
npm install
```

Con este se instalaran todas las dependencias necesarias para el proyecto en una carpeta llamada node_modules