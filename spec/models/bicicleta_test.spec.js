var Bicicleta = require('../../models/bicicleta');

//SE DEJA EN BLANCO BICICLETAS PARA TESTING
beforeEach(() => { 
    Bicicleta.allBicis = []; 
});

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-33.435014, -70.629709]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'rojo', 'urbana', [-33.435014, -70.629709]);
        var aBici2 = new Bicicleta(2, 'azul', 'montaña', [-33.435732, -70.640924]);

        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    });
});


describe('Bicicleta.removeById', () => {
    it('debe eliminar la bici con id 1', () => {

        //Verifica si en la colección existen Bicicletas (si existen falla)
        expect(Bicicleta.allBicis.length).toBe(0);

        //Agrega Bicicleta id=1
        var aBici = new Bicicleta(1, 'rojo', 'urbana', [-33.435014, -70.629709]);
        Bicicleta.add(aBici);

        //Remueve Bicicleta con id 1
        Bicicleta.removeById(aBici.id);

        expect(Bicicleta.allBicis.length).toBe(0);

    });
});